# Trabalho 2 - Sistemas Embarcados

# Compilação

Passo 1: Entrar na pasta 'codigo_fonte'
Passo 2: Utilizar o comando 'make'
Passo 3: Utilizar o comando 'make run'

# Vídeo de apresentação

O vídeo de apresentação se encontra na pasta 'video'

# Imagens do dashboard

## MODO CURVA
![CURVA](/imagens/CURVA.png)

## MODO MANUAL
![MANUAL01](/imagens/MANUAL_1.png)
![MANUAL02](/imagens/MANUAL_2.png)
![MANUAL03](/imagens/MANUAL_3.png)