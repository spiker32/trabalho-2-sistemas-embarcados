#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>//Used for UART
#include <termios.h>//Used for UART

#include <crc.h>
#include <constantes.h>
#include <comunicacao.h>

static char matricula[] = {0, 2, 6, 4};

Mensagem criar_nova_mensagem(char codigo, char sub_codigo) {
  Mensagem mensagem = {
    .uart_filestream = -1,
    .endereco = ENDERECO_SERVIDOR,
    .codigo = codigo,
    .sub_codigo = sub_codigo
  };
  return mensagem;
}

void iniciar_uart(Mensagem *mensagem) {
  mensagem->uart_filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);
  if (mensagem->uart_filestream == -1) {
    printf("Falha ao iniciar uart.\n");
  }

  struct termios options;
  tcgetattr(mensagem->uart_filestream, &options);
  options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
  options.c_iflag = IGNPAR;
  options.c_oflag = 0;
  options.c_lflag = 0;
  tcflush(mensagem->uart_filestream, TCIFLUSH);
  tcsetattr(mensagem->uart_filestream, TCSANOW, &options);
}

void fechar_uart(Mensagem *mensagem) {
  free(mensagem->mensagem);
  close(mensagem->uart_filestream);
}

void enviar_mensagem(Mensagem *mensagem, unsigned char *dados, int tamanho) {
  unsigned char buffer[100];
  int tamanho_mensagem = 7;
  buffer[0] = mensagem->endereco;
  buffer[1] = mensagem->codigo;
  buffer[2] = mensagem->sub_codigo;

  buffer[3] = matricula[0];
  buffer[4] = matricula[1];
  buffer[5] = matricula[2];
  buffer[6] = matricula[3];

  memcpy(&buffer[tamanho_mensagem], dados, tamanho);
  tamanho_mensagem += tamanho;

  int tamanho_crc = 2;
  mensagem->crc = calcula_CRC(buffer, tamanho_mensagem);

  memcpy(&buffer[tamanho_mensagem], &mensagem->crc, tamanho_crc);
  tamanho_mensagem += tamanho_crc;

  int result = write(mensagem->uart_filestream, &buffer, tamanho_mensagem);
  if (result < 0) {
    printf("Erro ao escrever na uart.\n");
  }
}

int ler_mensagem(Mensagem *mensagem) {
  int tamanho_mensagem = 100;

  mensagem->mensagem = malloc(sizeof(unsigned char) * tamanho_mensagem);
  int tamanho_leitura = read(mensagem->uart_filestream, (void *)mensagem->mensagem, tamanho_mensagem);

  if(tamanho_leitura < 0) {
    printf("Erro na leitura.\n");
    return -1;
  }
  else if (tamanho_leitura == 0) {
    //printf("Tamanho de leitura = 0.\n");
    return 0;
  }
  else { 
    mensagem->mensagem[tamanho_leitura] = '\0';
  }

  if(validar_mensagem(mensagem) == 1){
    return 1;
  }

  return -1;
}

float ler_float(Mensagem *mensagem) {
  float resposta;
  memcpy(&resposta, &mensagem->mensagem[3], sizeof(resposta));
  return resposta;
}

int ler_int(Mensagem *mensagem) {
  int resposta;
  memcpy(&resposta, &mensagem->mensagem[3], sizeof(resposta));
  return resposta;
}

int validar_mensagem(Mensagem *mensagem) {
  short crc;
  memcpy(&crc, &mensagem->mensagem[7], sizeof(crc));

  short crcMensagem = calcula_CRC(mensagem->mensagem, 7);

  if (crc != crcMensagem) {
    printf("%d CRC inválido %d\n", crcMensagem, crc);
    return -1;
  }
  if (mensagem->codigo != mensagem->mensagem[1] || mensagem->sub_codigo != mensagem->mensagem[2]) {
    printf("Mensagem inválida\n");
    return -1;
  }

  //printf("Mensagem válida\n");
  return 1;
}