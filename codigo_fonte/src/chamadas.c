#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>//Used for UART
#include <termios.h>//Used for UART

#include <constantes.h>
#include <comunicacao.h>
#include <chamadas.h>

float get_temperaturas(int tipo) {
    unsigned char dados;
    int tamanho = 0;
    Mensagem minhaMensagem;
    float resposta;

    if(tipo == 1){
        minhaMensagem = criar_nova_mensagem(CODIGO_REQUISICAO, PEDIR_TEMPERATURA_INTERNA);
    }
    else if(tipo == 2){
        minhaMensagem = criar_nova_mensagem(CODIGO_REQUISICAO, PEDIR_TEMPERATURA_REFERENCIA);
    }

    iniciar_uart(&minhaMensagem);
    enviar_mensagem(&minhaMensagem, &dados, tamanho);
    sleep(1);

    if(ler_mensagem(&minhaMensagem) == 1){
        resposta = ler_float(&minhaMensagem);
    }   
    
    fechar_uart(&minhaMensagem);

    return resposta;
}

int get_comando_usuario() {
    unsigned char dados;
    int tamanho = 0;
    Mensagem minhaMensagem;
    int resposta;

    minhaMensagem = criar_nova_mensagem(CODIGO_REQUISICAO, LER_COMANDO_USUARIO);

    iniciar_uart(&minhaMensagem);
    enviar_mensagem(&minhaMensagem, &dados, tamanho);
    sleep(1);

    if(ler_mensagem(&minhaMensagem) == 1){
        resposta = ler_int(&minhaMensagem);
    }   
    
    fechar_uart(&minhaMensagem);

    return resposta;
}

int post_mensagem(unsigned char* dado, int tamanho_dado, char codigo, char sub_codigo) {
    Mensagem minhaMensagem;
    minhaMensagem = criar_nova_mensagem(codigo, sub_codigo);

    iniciar_uart(&minhaMensagem);
    enviar_mensagem(&minhaMensagem, dado, tamanho_dado);
    sleep(1);

    int resposta_mensagem = ler_mensagem(&minhaMensagem);
    if(resposta_mensagem == 1 || resposta_mensagem == 0){
        fechar_uart(&minhaMensagem);
        return 1;
    }

    fechar_uart(&minhaMensagem); 
    return -1;
}

