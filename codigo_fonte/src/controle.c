#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h> //Used for UART
#include <termios.h> //Used for UART
#include <pthread.h>
#include <wiringPi.h>
#include <softPwm.h>

#include <comunicacao.h>
#include <controle.h>
#include <chamadas.h>
#include <constantes.h>
#include <crc.h>
#include <pid.h>

void controle_curva() {
    float temperaturas[] = {25, 38, 46, 54, 57, 61, 63, 54, 33, 25};
    int tempos[] =         {40, 60, 60, 120, 20, 40, 60, 60, 60, 120};

    for(int i = 0; i < 10; i++) {
        for(int j = 0; j < tempos[i]; j++) {
            envia_temperatura_referencia(temperaturas[i]);
            pid_atualiza_referencia(temperaturas[i]);
            controle_manual(0); 
            j+=2;
        }
    }
}

void envia_temperatura_referencia(float temperatura) {
    int resultado_chamada = -1;
    while(resultado_chamada != 1) {
        resultado_chamada = post_mensagem((unsigned char*)&temperatura, sizeof(int), CODIGO_ENVIO, ENVIAR_SINAL_REFERENCIA);
    }
}

void controle_manual(int atualiza_referencia) {
    if(atualiza_referencia == 1){
        pid_atualiza_referencia(get_temperaturas(2));
    }

    float temperatura_interna = get_temperaturas(1);

    int resultado_pid = (int) pid_controle((double)temperatura_interna);

    if(resultado_pid > -40 && 0 > resultado_pid){
        resultado_pid = -40;
    }

    int resultado_chamada = -1;
    
    if(resultado_pid < 0) {
        while(resultado_chamada != 1) {
            resultado_chamada = post_mensagem((unsigned char*)&resultado_pid, sizeof(int), CODIGO_ENVIO, ENVIAR_SINAL_CONTROLE);
        }
        resultado_pid = resultado_pid * -1;
        atualiza_ventoinha((double)resultado_pid);
        sleep(0.1);
        atualiza_resistor(0);
    }
    else {
        while(resultado_chamada != 1) {
            resultado_chamada = post_mensagem((unsigned char*)&resultado_pid, sizeof(int), CODIGO_ENVIO, ENVIAR_SINAL_CONTROLE);
        }
        atualiza_resistor((double)resultado_pid);
        sleep(0.1);
        atualiza_ventoinha(0);
    }    
}

void inicia_gpio() {
    if(wiringPiSetup() != 0){
        printf("Erro ao iniciar GPIO\n");
    }
    pinMode(GPIO_RESISTOR, OUTPUT);
    pinMode(GPIO_VENTOINHA, OUTPUT);
    softPwmCreate(GPIO_RESISTOR, 0, 100);
    softPwmCreate(GPIO_VENTOINHA, 0, 100);
}

void atualiza_resistor(double valor) {
    //printf("RESISTOR: %lf\n", valor);
    softPwmWrite(GPIO_RESISTOR, valor);
}

void atualiza_ventoinha(double valor) {
    //printf("VENTOINHA: %lf\n", valor);
    softPwmWrite(GPIO_VENTOINHA, valor);
}

void parar_gpio() {
    softPwmStop(GPIO_RESISTOR);
    softPwmStop(GPIO_VENTOINHA);
}