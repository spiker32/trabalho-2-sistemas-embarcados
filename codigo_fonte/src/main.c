#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h> //Used for UART
#include <termios.h> //Used for UART
#include <signal.h>
#include <pthread.h>

#include <pid.h>
#include <chamadas.h>
#include <comunicacao.h>
#include <constantes.h>
#include <controle.h>

unsigned char MODO_CURVA = 0;
unsigned char ESTADO_SISTEMA = 0;
unsigned char FUNCIONAMENTO_SISTEMA = 0;

static volatile int roda_programa = 1;

void ler_comandos_usuario();
void iniciar_estado_sistema();
void pararPrograma();

int main(int argc, char *argv[]) {
    int menu = 0;
    printf("Digite uma opção:\n 1 - Usar KP, KI e KD padrão\n 2 - Digitar manualmente\n");
    scanf("%d", &menu);

    if(menu == 2){
        double kp, ki, kd;
        printf("Digite o KP\n");
        scanf("%lf", &kp);
        printf("Digite o KI\n");
        scanf("%lf", &ki);
        printf("Digite o KD\n");
        scanf("%lf", &kd);

        pid_configura_constantes(kp, ki, kd);
    }

    iniciar_estado_sistema();
    signal(SIGINT, pararPrograma);
    while (roda_programa == 1) {
        ler_comandos_usuario();
        if(ESTADO_SISTEMA == 1 && FUNCIONAMENTO_SISTEMA == 1){
            if(MODO_CURVA == 0){
                controle_manual(1);
            }
            else {
                controle_curva();
            }
        }
    }
    
    return 0;
}

void ler_comandos_usuario() {
    int comando_usuario;
    comando_usuario = get_comando_usuario();
    switch (comando_usuario) {
        case LIGA_FORNO:
            ESTADO_SISTEMA = 1;
            post_mensagem(&ESTADO_SISTEMA, 1, CODIGO_ENVIO, ENVIAR_ESTADO_SISTEMA);
            break;
        case DESLIGA_FORNO:
            ESTADO_SISTEMA = 0;
            post_mensagem(&ESTADO_SISTEMA, 1, CODIGO_ENVIO, ENVIAR_ESTADO_SISTEMA);
            break;
        case INICIA_AQUECIMENTO:
            if(ESTADO_SISTEMA == 1 && FUNCIONAMENTO_SISTEMA == 0) {
                FUNCIONAMENTO_SISTEMA = 1;
                post_mensagem(&FUNCIONAMENTO_SISTEMA, 1, CODIGO_ENVIO, ENVIAR_ESTADO_FUNCIONAMENTO);
            }
            break;
        case CANCELA_PROCESSO:
            FUNCIONAMENTO_SISTEMA = 0;
            post_mensagem(&FUNCIONAMENTO_SISTEMA, 1, CODIGO_ENVIO, ENVIAR_ESTADO_FUNCIONAMENTO);
            break;
        case ALTERNA_MODO:
            if(MODO_CURVA == 0){
                MODO_CURVA = 1;
            }
            else {
                MODO_CURVA = 0;
            }
            post_mensagem(&MODO_CURVA, 1, CODIGO_ENVIO, ENVIAR_MODO_CURVA);
            break;
        default:
            break;
    }
}

void iniciar_estado_sistema() {
    inicia_gpio();
    unsigned char desligado = 0;
    post_mensagem(&desligado, 1, CODIGO_ENVIO, ENVIAR_ESTADO_SISTEMA);
    post_mensagem(&desligado, 1, CODIGO_ENVIO, ENVIAR_ESTADO_FUNCIONAMENTO);
    post_mensagem(&desligado, 1, CODIGO_ENVIO, ENVIAR_MODO_CURVA);
}

void pararPrograma() {
    atualiza_resistor(0);
    atualiza_ventoinha(0);
    parar_gpio();
    roda_programa = 0;
}