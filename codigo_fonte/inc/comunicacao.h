#ifndef COMUNICACAO_H_
#define COMUNICACAO_H_

typedef struct Mensagem
{
  int uart_filestream;
  char endereco;
  char codigo;
  char sub_codigo;
  short crc;
  unsigned char *mensagem;
} Mensagem;

Mensagem criar_nova_mensagem(char codigo, char sub_codigo);
void iniciar_uart(Mensagem *mensagem);
void fechar_uart(Mensagem *mensagem);
void enviar_mensagem(Mensagem *mensagem, unsigned char *dados, int tamanho);
int ler_mensagem(Mensagem *mensagem);
float ler_float(Mensagem *mensagem);
int ler_int(Mensagem *mensagem);
int validar_mensagem(Mensagem *mensagem);

#endif