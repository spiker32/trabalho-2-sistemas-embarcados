#ifndef CONTROLE_H_
#define CONTROLE_H_

void controle_curva();
void envia_temperatura_referencia(float temperatura);
void controle_manual(int atualiza_referencia);
void inicia_gpio();
void atualiza_resistor(double valor);
void atualiza_ventoinha(double valor);
void parar_gpio();

#endif